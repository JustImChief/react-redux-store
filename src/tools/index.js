import { isFunction, isString } from 'ts-data-validator';
import { getFormValues }        from 'redux-form';
import { connect }              from 'react-redux';

export const _connectReducers = (store, reducerManager) => (...reducersOrFormName) => {
  const {reducers, formName} = reducersOrFormName.reduce((accumulator, currentValue) => {
    if (isFunction(currentValue)) {
      _registerReducer(store, reducerManager)(currentValue);
      accumulator.reducers.push(currentValue);
    } else if (isString(currentValue)) {
      accumulator.formName = currentValue;
    }

    return accumulator;
  }, {reducers: []});

  function mapStateToProps(state) {
    const props = reducers.reduce((accumulator, currentValue) => ({
      ...accumulator,
      [currentValue.name]: state[currentValue.name] || {},
    }), {});

    if (isString(formName)) {
      props.formValues = getFormValues(formName)(state) || {};
    }

    return {...props};
  }

  return connect(mapStateToProps);
};

export const _dispatch = (store) => store.dispatch;

export const _getState = (store) => (reducer) => {
  const state = store.getState();

  return state[reducer] || {};
};

export const _registerReducer = (store, reducerManager) => (Reducer) => {
  store.replaceReducer(reducerManager.add(Reducer.name, new Reducer().init()));
};

export const _unregisterReducer = (store, reducerManager) => (Reducer) => {
  store.replaceReducer(reducerManager.remove(isString(Reducer) ? Reducer : Reducer.name));
};
