import thunkMiddleware                    from 'redux-thunk';
import { applyMiddleware, createStore }   from 'redux';
import { composeWithDevTools }            from 'redux-devtools-extension';
import { getFormValues, reducer as form } from 'redux-form';
import { connect }                        from 'react-redux';
import { isFunction, isString }           from 'ts-data-validator';

import ReducerManager from './ReducerManager';

class ReduxStore {
  manager;
  middleware = process.env.NODE_ENV === 'production'
    ? applyMiddleware(thunkMiddleware)
    : composeWithDevTools(applyMiddleware(thunkMiddleware));
  storage;

  constructor() {
    this.reducerManager = new ReducerManager({form});
    this.store          = createStore(this.reducerManager.reduce, this.middleware);
  }

  get dispatch() {
    return this.store.dispatch;
  }

  get reducerManager() {
    return this.manager;
  }

  set reducerManager(reducerManager) {
    this.manager = reducerManager;
  }

  get store() {
    return this.storage;
  }

  set store(storage) {
    this.storage = storage;
  }

  connectReducers = (...reducersOrFormName) => {
    const {reducers, formName} = reducersOrFormName.reduce((accumulator, currentValue) => {
      if (isFunction(currentValue)) {
        this.registerReducer(currentValue);

        accumulator.reducers.push(currentValue);
      } else if (isString(currentValue)) {
        accumulator.formName = currentValue;
      }

      return accumulator;
    }, {reducers: []});

    return connect((state) => {
      const props = reducers.reduce((accumulator, currentValue) => ({
        ...accumulator,
        [currentValue._name]: state[currentValue._name] || {},
      }), {});

      console.log({'@jclib/react-redux-store': props});

      if (isString(formName)) {
        props.formValues = getFormValues(formName)(state) || {};
      }

      return props;
    });
  };

  getState = (reducer) => {
    if (!this.store) {
      return {};
    }

    const state = this.store?.getState() || {};

    return state[reducer] || {};
  };

  registerReducer = (Reducer) => {
    this.store.replaceReducer(this.reducerManager.add(Reducer._name, new Reducer().init()));
  };

  ssr = (data) => {
    const reducers     = {};
    const initialState = {};

    for (let reducer of data) {
      let [Reducer, initial] = reducer;

      reducers[Reducer._name]     = new Reducer().init();
      initialState[Reducer._name] = {
        ...reducers[Reducer._name],
        ...initial,
      };
    }

    this.reducerManager = new ReducerManager({form, ...reducers});
    this.store          = createStore(this.reducerManager.reduce, initialState, this.middleware);

    return this.store;
  };

  unregisterReducer = (Reducer) => {
    this.store.replaceReducer(this.reducerManager.remove(isString(Reducer) ? Reducer : Reducer._name));
  };
}

const storage = new ReduxStore();

const {
        connectReducers,
        dispatch,
        getState,
        registerReducer,
        store,
        ssr,
        unregisterReducer,
      } = storage;

export default store;
export {
  dispatch,
  connectReducers,
  getState,
  registerReducer,
  ssr,
  unregisterReducer,
};