import { combineReducers } from 'redux';

class ReducerManager {
  constructor(initialReducer = {}) {
    this.reducer = {...initialReducer};
    this.combineReducer = combineReducers(this.reducer);
    this.keysToRemove   = [];
  }

  add = (key, reducer) => {
    if ((key && !this.reducer.hasOwnProperty(key))) {
      this.reducer[key]   = reducer;
      this.combineReducer = combineReducers(this.reducer);
    }

    return this.combineReducer;
  };

  getReducerMap = () => {
    return this.reducer;
  };

  reduce = (state, action) => {
    const newState = {...state};

    if (this.keysToRemove.length > 0) {
      for (let key of this.keysToRemove) {
        delete newState[key];
      }

      this.keysToRemove = [];
    }

    return this.combineReducer(newState, action);
  };

  remove = (key) => {
    if (key && this.reducer.hasOwnProperty(key)) {
      delete this.reducer[key];

      this.keysToRemove.push(key);
      this.combineReducer = combineReducers(this.reducer);
    }

    return this.combineReducer;
  };
}

export default ReducerManager;
