"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "types", {
  enumerable: true,
  get: function get() {
    return _types.default;
  }
});
exports.default = void 0;

var _tsDataTransformer = _interopRequireDefault(require("ts-data-transformer"));

var _tsDataValidator = _interopRequireDefault(require("ts-data-validator"));

var _types = _interopRequireDefault(require("./types"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class Reducer {
  static get initialState() {
    return new Reducer().initialState;
  }

  constructor() {
    var _this = this;

    var _initialState = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

    _defineProperty(this, "actions", {});

    _defineProperty(this, "DT", _tsDataTransformer.default);

    _defineProperty(this, "DV", _tsDataValidator.default);

    _defineProperty(this, "initialData", {});

    _defineProperty(this, "initialState", {
      initialStateLoaded: false,
      loading: false,
      wait: false
    });

    _defineProperty(this, "initialValues", {});

    _defineProperty(this, "failure", function (state, action) {
      var wait = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
      return _objectSpread(_objectSpread({}, state), {}, {
        [wait ? 'wait' : 'loading']: false
      });
    });

    _defineProperty(this, "getValue", (valueType, value) => {
      if (this.DV.isObject(valueType)) {
        return Object.keys(valueType).reduce((result, key) => _objectSpread(_objectSpread({}, result), {}, {
          [key]: this.getValue(valueType[key], (value || {})[key])
        }), {});
      }

      if (this.DV.isArray(valueType)) {
        var [type, defaultValue] = valueType;

        switch (type) {
          case _types.default.array:
            return this.DT.toArray(value, defaultValue);

          case _types.default.bool:
            return this.DT.toBoolean(value, defaultValue);

          case _types.default.number:
            return this.DT.toNumber(value, defaultValue);

          case _types.default.object:
            return this.DT.toObject(value, defaultValue);

          case _types.default.string:
            return this.DT.toString(value, defaultValue);
        }
      }

      switch (valueType) {
        case _types.default.array:
          return this.DT.toArray(value, []);

        case _types.default.bool:
          return this.DT.toBoolean(value, false);

        case _types.default.number:
          return this.DT.toNumber(value, null);

        case _types.default.object:
          return this.DT.toObject(value, {});

        case _types.default.string:
          return this.DT.toString(value, null);

        default:
          return null;
      }
    });

    _defineProperty(this, "init", () => {
      return this.reducer();
    });

    _defineProperty(this, "prepareValues", function (initial) {
      var values = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      return Object.keys(initial).reduce((result, field) => _objectSpread(_objectSpread({}, result), {}, {
        [field]: _this.getValue(initial[field], values[field])
      }), {});
    });

    _defineProperty(this, "reducer", () => function () {
      var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : _this.initialState;
      var action = arguments.length > 1 ? arguments[1] : undefined;

      if (_this.actions.hasOwnProperty(action.type)) {
        return _this.actions[action.type](state, action);
      }

      return _objectSpread({}, state);
    });

    _defineProperty(this, "request", function (state, action) {
      var wait = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
      return _objectSpread(_objectSpread({}, state), {}, {
        [wait ? 'wait' : 'loading']: true
      });
    });

    _defineProperty(this, "ssr", function () {
      var initialState = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      _this.initialState = _objectSpread(_objectSpread({}, _this.initialState), initialState);
      return _this.init();
    });

    _defineProperty(this, "success", (state, action, wait) => _objectSpread(_objectSpread({}, state), {}, {
      [wait ? 'wait' : 'loading']: false
    }));

    this.initialData = (_initialState === null || _initialState === void 0 ? void 0 : _initialState.data) || {};
    this.initialValues = (_initialState === null || _initialState === void 0 ? void 0 : _initialState.values) || {};
    this.initialState = _objectSpread(_objectSpread({}, this.initialState), {}, {
      data: this.prepareValues(this.initialData),
      values: this.prepareValues(this.initialValues)
    });
    this.init();
  }

}

_defineProperty(Reducer, "_name", 'abstract');

var _default = Reducer;
exports.default = _default;