"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

/**
 * @type {{
 *   number: string,
 *   bool:   string,
 *   string: string,
 *   array:  string,
 *   object: string,
 * }}
 */
var types = {
  array: 'array',
  bool: 'bool',
  number: 'number',
  object: 'object',
  string: 'string'
};
var _default = types;
exports.default = _default;