"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports._unregisterReducer = exports._registerReducer = exports._getState = exports._dispatch = exports._connectReducers = void 0;

var _tsDataValidator = require("ts-data-validator");

var _reduxForm = require("redux-form");

var _reactRedux = require("react-redux");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var _connectReducers = (store, reducerManager) => function () {
  for (var _len = arguments.length, reducersOrFormName = new Array(_len), _key = 0; _key < _len; _key++) {
    reducersOrFormName[_key] = arguments[_key];
  }

  var {
    reducers,
    formName
  } = reducersOrFormName.reduce((accumulator, currentValue) => {
    if ((0, _tsDataValidator.isFunction)(currentValue)) {
      _registerReducer(store, reducerManager)(currentValue);

      accumulator.reducers.push(currentValue);
    } else if ((0, _tsDataValidator.isString)(currentValue)) {
      accumulator.formName = currentValue;
    }

    return accumulator;
  }, {
    reducers: []
  });

  function mapStateToProps(state) {
    var props = reducers.reduce((accumulator, currentValue) => _objectSpread(_objectSpread({}, accumulator), {}, {
      [currentValue.name]: state[currentValue.name] || {}
    }), {});

    if ((0, _tsDataValidator.isString)(formName)) {
      props.formValues = (0, _reduxForm.getFormValues)(formName)(state) || {};
    }

    return _objectSpread({}, props);
  }

  return (0, _reactRedux.connect)(mapStateToProps);
};

exports._connectReducers = _connectReducers;

var _dispatch = store => store.dispatch;

exports._dispatch = _dispatch;

var _getState = store => reducer => {
  var state = store.getState();
  return state[reducer] || {};
};

exports._getState = _getState;

var _registerReducer = (store, reducerManager) => Reducer => {
  store.replaceReducer(reducerManager.add(Reducer.name, new Reducer().init()));
};

exports._registerReducer = _registerReducer;

var _unregisterReducer = (store, reducerManager) => Reducer => {
  store.replaceReducer(reducerManager.remove((0, _tsDataValidator.isString)(Reducer) ? Reducer : Reducer.name));
};

exports._unregisterReducer = _unregisterReducer;