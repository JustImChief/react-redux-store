"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _redux = require("redux");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class ReducerManager {
  constructor() {
    var initialReducer = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

    _defineProperty(this, "add", (key, reducer) => {
      if (key && !this.reducer.hasOwnProperty(key)) {
        this.reducer[key] = reducer;
        this.combineReducer = (0, _redux.combineReducers)(this.reducer);
      }

      return this.combineReducer;
    });

    _defineProperty(this, "getReducerMap", () => {
      return this.reducer;
    });

    _defineProperty(this, "reduce", (state, action) => {
      var newState = _objectSpread({}, state);

      if (this.keysToRemove.length > 0) {
        for (var key of this.keysToRemove) {
          delete newState[key];
        }

        this.keysToRemove = [];
      }

      return this.combineReducer(newState, action);
    });

    _defineProperty(this, "remove", key => {
      if (key && this.reducer.hasOwnProperty(key)) {
        delete this.reducer[key];
        this.keysToRemove.push(key);
        this.combineReducer = (0, _redux.combineReducers)(this.reducer);
      }

      return this.combineReducer;
    });

    this.reducer = _objectSpread({}, initialReducer);
    this.combineReducer = (0, _redux.combineReducers)(this.reducer);
    this.keysToRemove = [];
  }

}

var _default = ReducerManager;
exports.default = _default;