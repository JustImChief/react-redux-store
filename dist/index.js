"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.unregisterReducer = exports.ssr = exports.registerReducer = exports.getState = exports.connectReducers = exports.dispatch = exports.default = void 0;

var _reduxThunk = _interopRequireDefault(require("redux-thunk"));

var _redux = require("redux");

var _reduxDevtoolsExtension = require("redux-devtools-extension");

var _reduxForm = require("redux-form");

var _reactRedux = require("react-redux");

var _tsDataValidator = require("ts-data-validator");

var _ReducerManager = _interopRequireDefault(require("./ReducerManager"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class ReduxStore {
  constructor() {
    var _this = this;

    _defineProperty(this, "manager", void 0);

    _defineProperty(this, "middleware", process.env.NODE_ENV === 'production' ? (0, _redux.applyMiddleware)(_reduxThunk.default) : (0, _reduxDevtoolsExtension.composeWithDevTools)((0, _redux.applyMiddleware)(_reduxThunk.default)));

    _defineProperty(this, "storage", void 0);

    _defineProperty(this, "connectReducers", function () {
      for (var _len = arguments.length, reducersOrFormName = new Array(_len), _key = 0; _key < _len; _key++) {
        reducersOrFormName[_key] = arguments[_key];
      }

      var {
        reducers,
        formName
      } = reducersOrFormName.reduce((accumulator, currentValue) => {
        if ((0, _tsDataValidator.isFunction)(currentValue)) {
          _this.registerReducer(currentValue);

          accumulator.reducers.push(currentValue);
        } else if ((0, _tsDataValidator.isString)(currentValue)) {
          accumulator.formName = currentValue;
        }

        return accumulator;
      }, {
        reducers: []
      });
      return (0, _reactRedux.connect)(state => {
        var props = reducers.reduce((accumulator, currentValue) => _objectSpread(_objectSpread({}, accumulator), {}, {
          [currentValue._name]: state[currentValue._name] || {}
        }), {});
        console.log({
          '@jclib/react-redux-store': props
        });

        if ((0, _tsDataValidator.isString)(formName)) {
          props.formValues = (0, _reduxForm.getFormValues)(formName)(state) || {};
        }

        return props;
      });
    });

    _defineProperty(this, "getState", reducer => {
      var _this$store;

      if (!this.store) {
        return {};
      }

      var state = ((_this$store = this.store) === null || _this$store === void 0 ? void 0 : _this$store.getState()) || {};
      return state[reducer] || {};
    });

    _defineProperty(this, "registerReducer", Reducer => {
      this.store.replaceReducer(this.reducerManager.add(Reducer._name, new Reducer().init()));
    });

    _defineProperty(this, "ssr", data => {
      var reducers = {};
      var initialState = {};

      for (var reducer of data) {
        var [Reducer, initial] = reducer;
        reducers[Reducer._name] = new Reducer().init();
        initialState[Reducer._name] = _objectSpread(_objectSpread({}, reducers[Reducer._name]), initial);
      }

      this.reducerManager = new _ReducerManager.default(_objectSpread({
        form: _reduxForm.reducer
      }, reducers));
      this.store = (0, _redux.createStore)(this.reducerManager.reduce, initialState, this.middleware);
      return this.store;
    });

    _defineProperty(this, "unregisterReducer", Reducer => {
      this.store.replaceReducer(this.reducerManager.remove((0, _tsDataValidator.isString)(Reducer) ? Reducer : Reducer._name));
    });

    this.reducerManager = new _ReducerManager.default({
      form: _reduxForm.reducer
    });
    this.store = (0, _redux.createStore)(this.reducerManager.reduce, this.middleware);
  }

  get dispatch() {
    return this.store.dispatch;
  }

  get reducerManager() {
    return this.manager;
  }

  set reducerManager(reducerManager) {
    this.manager = reducerManager;
  }

  get store() {
    return this.storage;
  }

  set store(storage) {
    this.storage = storage;
  }

}

var storage = new ReduxStore();
var {
  connectReducers,
  dispatch,
  getState,
  registerReducer,
  store,
  ssr,
  unregisterReducer
} = storage;
exports.unregisterReducer = unregisterReducer;
exports.ssr = ssr;
exports.registerReducer = registerReducer;
exports.getState = getState;
exports.dispatch = dispatch;
exports.connectReducers = connectReducers;
var _default = store;
exports.default = _default;