import DataTransformer from 'ts-data-transformer';
import DataValidator   from 'ts-data-validator';

import types from './types';

class Reducer {
  static _name = 'abstract';

  static get initialState() {
    return new Reducer().initialState;
  };

  actions = {};

  DT = DataTransformer;
  DV = DataValidator;

  initialData   = {};
  initialState  = {
    initialStateLoaded: false,
    loading:            false,
    wait:               false,
  };
  initialValues = {};

  constructor(initialState = {}) {
    this.initialData   = initialState?.data || {};
    this.initialValues = initialState?.values || {};

    this.initialState = {
      ...this.initialState,
      data:   this.prepareValues(this.initialData),
      values: this.prepareValues(this.initialValues),
    };

    this.init();
  }

  failure = (state, action, wait = false) => ({
    ...state,
    [wait ? 'wait' : 'loading']: false,
  });

  getValue = (valueType, value) => {
    if (this.DV.isObject(valueType)) {
      return Object.keys(valueType).reduce((result, key) => ({
        ...result,
        [key]: this.getValue(valueType[key], (value || {})[key]),
      }), {});
    }

    if (this.DV.isArray(valueType)) {
      const [type, defaultValue] = valueType;

      switch (type) {
        case types.array:
          return this.DT.toArray(value, defaultValue);

        case types.bool:
          return this.DT.toBoolean(value, defaultValue);

        case types.number:
          return this.DT.toNumber(value, defaultValue);

        case types.object:
          return this.DT.toObject(value, defaultValue);

        case types.string:
          return this.DT.toString(value, defaultValue);
      }
    }

    switch (valueType) {
      case types.array:
        return this.DT.toArray(value, []);

      case types.bool:
        return this.DT.toBoolean(value, false);

      case types.number:
        return this.DT.toNumber(value, null);

      case types.object:
        return this.DT.toObject(value, {});

      case types.string:
        return this.DT.toString(value, null);

      default:
        return null;
    }
  };

  init = () => {
    return this.reducer();
  };

  prepareValues = (initial, values = {}) => {
    return Object.keys(initial).reduce((result, field) => ({
      ...result,
      [field]: this.getValue(initial[field], values[field]),
    }), {});
  };

  reducer = () => (state = this.initialState, action) => {
    if (this.actions.hasOwnProperty(action.type)) {
      return this.actions[action.type](state, action);
    }

    return {...state};
  };

  request = (state, action, wait = false) => ({
    ...state,
    [wait ? 'wait' : 'loading']: true,
  });

  ssr = (initialState = {}) => {
    this.initialState = {
      ...this.initialState,
      ...initialState,
    };

    return this.init();
  };

  success = (state, action, wait) => ({
    ...state,
    [wait ? 'wait' : 'loading']: false,
  });
}

export default Reducer;
export { types };