/**
 * @type {{
 *   number: string,
 *   bool:   string,
 *   string: string,
 *   array:  string,
 *   object: string,
 * }}
 */
const types = {
  array:  'array',
  bool:   'bool',
  number: 'number',
  object: 'object',
  string: 'string',
};

export default types;